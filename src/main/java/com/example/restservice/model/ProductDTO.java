package com.example.restservice.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class ProductDTO {
    @NotBlank(message = "Name field is mandatory")
    private String name;
    @Pattern(regexp = "^.{13}$", message = "There should be exactly 13 characters")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
