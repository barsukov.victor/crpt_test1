package com.example.restservice.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class DocumentDTO {
    @Pattern(regexp = "^.{9}$", message = "There should be exactly 9 characters")
    @NotBlank(message = "Seller field is mandatory")
    private String seller;
    @Pattern(regexp = "^.{9}$", message = "There should be exactly 9 characters")
    @NotBlank(message = "Customer field is mandatory")
    private String customer;
    @NotNull(message = "Products field is mandatory")
    @Size(min = 1, message = "There should be at least 1 product")
    @Valid
    private List<ProductDTO> products;

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

}
