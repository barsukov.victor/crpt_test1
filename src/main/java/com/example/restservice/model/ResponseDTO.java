package com.example.restservice.model;

public class ResponseDTO {
    private Integer code = 200;
    private String errorMessage;

    public ResponseDTO() {
    }

    public ResponseDTO(Integer code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
