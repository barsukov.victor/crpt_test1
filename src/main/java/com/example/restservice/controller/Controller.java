package com.example.restservice.controller;

import com.example.restservice.model.DocumentDTO;
import com.example.restservice.model.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
public class Controller {


    @PostMapping("/doc")
    public ResponseDTO doc(@Valid @RequestBody DocumentDTO document) {
        return new ResponseDTO();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseDTO handleValidationExceptions(
        MethodArgumentNotValidException ex) {
        String errorMessage = ex.getBindingResult().getAllErrors().stream()
            .map(s -> ((FieldError) s).getField() + " = " + s.getDefaultMessage())
            .collect(Collectors.joining(";"));
        return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }
}
